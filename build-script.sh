#!/bin/bash

set -e

# setup clang-3.6
wget http://releases.llvm.org/3.6.2/clang+llvm-3.6.2-x86_64-linux-gnu-ubuntu-14.04.tar.xz
tar xvif clang+llvm-3.6.2-x86_64-linux-gnu-ubuntu-14.04.tar.xz
ln -sf clang+llvm-3.6.2-x86_64-linux-gnu-ubuntu-14.04 clang

git clone https://bitbucket.org/sixujie/chc-study.git seahorn

cd /seahorn/seahorn ; mkdir build ; cd build

# configure seahorn
cmake -DCMAKE_INSTALL_PREFIX=run -DCMAKE_CXX_COMPILER=/seahorn/clang/bin/clang++ -DCMAKE_C_COMPILER=/seahorn/clang/bin/clang  ../ -DCUSTOM_BOOST_ROOT=/deps/boost/ -GNinja

# biuld llvm and z3
ninja
# download extra packages
ninja extra

# reset requested repos
cd ../llvm-seahorn/ && git reset --hard 39aa187 && cd ../llvm-dsa/ && git reset --hard fedb3e3 && cd ../sea-dsa/ && git reset --hard 246f0f5 && cd ../crab-llvm/ && git reset --hard e2fac87 && cd ../build/
 
# turn crab off
mv ../crab-llvm ../crab-llvm.off

cmake ../

# compile seahorn 
ninja install

# install clang-3.6 for seahorn to find
ln -sf /seahorn/clang/bin/clang ./run/bin/clang
ln -sf /seahorn/clang/bin/clang++ ./run/bin/clang++


# build libsvm
cd ../libsvm
make CXX=/seahorn/clang/bin/clang++ CFLAGS='-std=c++11 -I/deps/boost/include -Wall -O3 -fPIC -Wno-c++11-extensions'

# build decision tree learner C50
cd ../C50/
make clean
make -j16

