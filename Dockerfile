#
#

# Pull base image.
FROM seahorn/seahorn-build-llvm5:xenial 

WORKDIR /seahorn

COPY build-script.sh /seahorn
   
RUN /bin/bash build-script.sh
